﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LLBML;
using LLBML.States;
using LLBML.Utils;
using Multiplayer;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GameChat
{
    class ChatBehavior : MonoBehaviour
    {
        private const int maxMessages = 5;
        private const float activeTime = 15.0f;

        private Rect chatPosition => new Rect(Plugin.instance.offsetX.Value, Plugin.instance.offsetY.Value, 600, 400); 

        private Queue<ChatMessage> chatMessageQueue;
        private Queue<float> messageTimeQueue;
        
        public bool chatWindowOpen;
        public float lastPress = 0f;
        public float lastClose = 0f;

        string myName;
        string currText ="";

        

        private static bool InOptions => ModDependenciesUtils.InModOptions();
        private static bool CanOpen => GameStates.GetCurrent() == GameState.GAME || GameStates.GetCurrent() == GameState.LOBBY_ONLINE;

        public struct ChatMessage
        {
            public String sender;
            public String text;
            public Color color;

            public ChatMessage(string sender, string text, Color color)
            {
                this.sender = sender;
                this.text = text;
                this.color = color;
            }

            public ChatMessage(string sender, string text)
            {
                this.sender = sender;
                this.text = text;
                this.color = Color.black;
            }


            public ChatMessage(string text)
            {
                this.sender = null;
                this.text = text;
                this.color = Color.black;
            }

            public ChatMessage(string text, Color color)
            {
                this.sender = null;
                this.text = text;
                this.color = color;
            }
        }


        void Start()
        {
            chatMessageQueue = new Queue<ChatMessage>();
            messageTimeQueue = new Queue<float>();
            chatWindowOpen = false;
            myName = "Me";
        }

        public void CreateChat(ChatMessage msg)
        { 
            chatMessageQueue.Enqueue(msg);
            messageTimeQueue.Enqueue(Time.fixedTime);
        }

        public void CreateSystemChat(String text)
        {
            chatMessageQueue.Enqueue(new ChatMessage(text, Color.red));
            messageTimeQueue.Enqueue(Time.fixedTime);
        }

        public IEnumerator SendChat()
        {
            
            
            CreateChat(new ChatMessage(myName, currText));
            Plugin.instance.SendChat(currText);

            currText = "";
            yield break;
        }

        public IEnumerator OpenChat()
        {
            chatWindowOpen = true;
            yield break;
        }

        void OnGUI()
        {
            if (!Plugin.IS_GAME && !Plugin.IS_LOBBY && !InOptions) return;
            if (Plugin.IS_GAME_PAUSED)
            {
                return;
            }

            /* handle keypresses*/
            bool pressedKey = false;
            Event e = Event.current;
            if (e.type == EventType.KeyDown && e.keyCode == Plugin.instance.chatKey.Value)
            {
                if (!chatWindowOpen)
                {
                    StartCoroutine(OpenChat());
                    lastPress = Time.time;
                    e.Use();
                    
                    pressedKey = true;
                }              
                
            }
            if (e.type == EventType.KeyDown && e.keyCode == KeyCode.Return)
            {
                if (chatWindowOpen && GUI.GetNameOfFocusedControl() == "ChatEntry")
                {
                    chatWindowOpen = false;
                    GUI.FocusControl(null);
                    StartCoroutine(SendChat());
                    
                    e.Use();
                    pressedKey = true;

                    lastClose = Time.time;
                }
            }

            if (e.type == EventType.KeyDown && e.keyCode == KeyCode.Escape)
            {
                if (chatWindowOpen)
                {
                    lastClose = Time.time;
                }
                chatWindowOpen = false;
                e.Use();
                pressedKey = true;

                
            }

            if (pressedKey)
            {
                lastPress = Time.time;
                return;
            }

            
            /* Trim messages */
            if (chatMessageQueue.Count > maxMessages)
            {
                chatMessageQueue.Dequeue();
                messageTimeQueue.Dequeue();
            }

            if (messageTimeQueue.Count > 0)
            {
                if (Time.fixedTime - messageTimeQueue.Peek() > activeTime)
                {
                    chatMessageQueue.Dequeue();
                    messageTimeQueue.Dequeue();
                }
            }

            /* show in mod settings menu*/

            if (InOptions)
            {
                chatWindowOpen = false;
                chatMessageQueue.Clear();
                chatMessageQueue.Enqueue(new ChatMessage("The quick brown fox jumped over the lazy dog."));
                chatMessageQueue.Enqueue(new ChatMessage("Please report any issues with game chat, thank you", Color.red));
                chatMessageQueue.Enqueue(new ChatMessage("Hello world", Color.blue));
                chatMessageQueue.Enqueue(new ChatMessage("ggs, go again :)"));
                messageTimeQueue.Clear();
                messageTimeQueue.Enqueue(Time.fixedTime - 14.0f);
                messageTimeQueue.Enqueue(Time.fixedTime - 14.0f);
                messageTimeQueue.Enqueue(Time.fixedTime - 14.0f);
                messageTimeQueue.Enqueue(Time.fixedTime - 14.0f);
            }

            /* draw Chat area*/
            GUI.depth = 1;
            GUILayout.BeginArea(chatPosition);
            GUILayout.FlexibleSpace();
            
            for (int i = 0; i < chatMessageQueue.Count; i++)
            {
                ChatMessage msg = chatMessageQueue.ElementAt(i);

                string message;
                if (msg.sender == null)
                {
                    message = msg.text;
                }
                else
                {
                    message = msg.sender + ": " + msg.text;
                }
                GUIStyle sty = ChatStyle.labStyle;
                if (msg.color != Color.black)
                {
                    sty.normal.textColor = msg.color;
                }
                GUILayout.Label(message, sty);

            }
            // time delay is a stupid way to get the chatKey keypress not to input into the text field
            // TODO should find a better way
            if (chatWindowOpen && !pressedKey && Time.time - lastPress > 0.02f)
            {
                GUI.SetNextControlName("ChatEntry");

                currText = GUILayout.TextField(currText, 100, ChatStyle._textFieldStyle);
                GUI.FocusControl("ChatEntry");

            }
            

            GUILayout.EndArea();
        }

        public bool IsInputBoxActive()
        {
            return chatWindowOpen;
        }
    }
}
