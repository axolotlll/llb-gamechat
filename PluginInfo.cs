﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameChat
{
    class PluginInfo
    {
        public const string PLUGIN_ID = "com.gitlab.axolotlll.llb-gamechat";
        public const string PLUGIN_NAME = "GameChat";
        public const string PLUGIN_VERSION = "1.0.1";
    }
}
