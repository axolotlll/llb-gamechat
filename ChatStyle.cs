﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace GameChat
{
    public static class ChatStyle
    {
        static Dictionary<string, Texture2D> texColors = new Dictionary<string, Texture2D>();

        static ChatStyle()
        {
            texColors.Clear();
            texColors.Add("Yellow", ColorToTexture2D(new Color(1f, 0.968f, 0.3f)));
            texColors.Add("LightYellow", ColorToTexture2D(new Color(1f, 1f, 0.5f)));
            texColors.Add("DarkGray", ColorToTexture2D(new Color(0.145f, 0.145f, 0.145f)));
            texColors.Add("LightGray", ColorToTexture2D(new Color(0.5f, 0.5f, 0.5f)));
            texColors.Add("Black", ColorToTexture2D(new Color32(12, 12, 12, 255)));
            texColors.Add("White", ColorToTexture2D(new Color32(255, 255, 255, 255)));
        }


        public static GUIStyle mainStyle
        {
            get
            {
                GUIStyle gUIStyle = new GUIStyle()
                {
                    padding = new RectOffset(4, 4, 4, 4),
                };
                gUIStyle.normal.background = texColors["Black"];
                return gUIStyle;
            }
        }

        public static GUIStyle labStyle
        {
            get
            {
                GUIStyle gUIStyle = new GUIStyle(GUI.skin.label)
                {
                    fontSize = 17,
                    alignment = TextAnchor.MiddleLeft,
                };
                gUIStyle.normal.background = ColorToTexture2D(new Color(0,0,0,0.2f));
                gUIStyle.normal.textColor = new Color(1f, 0.968f, 0.3f);
                return gUIStyle;
            }
        }

       

        public static GUIStyle _textFieldStyle
        {
            get
            {
                GUIStyle gUIStyle = new GUIStyle()
                {
                    fontSize = 16,
                    padding = new RectOffset(4, 4, 4, 4),
                };
                gUIStyle.margin = new RectOffset(0, 0, 5, 0);
                gUIStyle.normal.textColor = new Color(1f, 0.968f, 0.3f);
                gUIStyle.normal.background = texColors["Black"];

                return gUIStyle;
            }
        }




        static Texture2D ColorToTexture2D(Color32 color)
        {
            Texture2D texture2D = new Texture2D(1, 1, TextureFormat.RGBA32, false);
            texture2D.SetPixel(0, 0, color);
            texture2D.Apply();
            return texture2D;
        }
    }

}
